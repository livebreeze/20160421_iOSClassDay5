//
//  AppDelegate.h
//  20160421_iOSClassDay5
//
//  Created by ChenSean on 4/21/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

