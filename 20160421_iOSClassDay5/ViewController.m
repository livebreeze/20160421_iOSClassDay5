//
//  ViewController.m
//  20160421_iOSClassDay5
//
//  Created by ChenSean on 4/21/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSMutableArray *list;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    list = [NSMutableArray new];
    
    MyData *data;
    // init data
    data = [[MyData alloc] initWithID:@"A01" andName:@"Sean測試測試測試測試測試測試" andAge:27];
    [list addObject:data];
    
    data = [[MyData alloc] initWithID:@"A02" andName:@"Tim" andAge:27];
    [list addObject:data];
    
    data = [[MyData alloc] initWithID:@"A03" andName:@"Sin" andAge:27];
    [list addObject:data];
    
    data = [[MyData alloc] initWithID:@"A04" andName:@"Stella" andAge:27];
    [list addObject:data];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    MyData *data = (MyData*)[list objectAtIndex:indexPath.row];
    UILabel *labelUserID = [cell viewWithTag:101];
    UILabel *labelUserName = [cell viewWithTag:103];
    UILabel *labelUserAge = [cell viewWithTag:102];
    labelUserID.text = data.userID;
    labelUserName.text = data.userName;
    labelUserAge.text = [NSString stringWithFormat:@"%d", data.userAge];
    
//    for (UIView *view in cell.contentView.subviews) {
//        if ([view isKindOfClass:[UILabel class]]) {
//            UILabel *label = (UILabel*) view;
//            
//            MyData *data = (MyData*)[list objectAtIndex:indexPath.row];
//            
//            switch (label.tag) {
//                    
//                case 101:
//                    label.text = data.userID;
//                    break;
//                    
//                case 102:
//                    label.text = [NSString stringWithFormat:@"%d", data.userAge];
//                    break;
//                
//                case 103:
//                    label.text = data.userName;                    
//                    break;
//                    
//                default:
//                    break;
//            }
//        }
//    }
    
    return cell;                                  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
