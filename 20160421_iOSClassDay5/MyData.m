//
//  MyData.m
//  20160421_iOSClassDay5
//
//  Created by ChenSean on 4/21/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "MyData.h"

@implementation MyData

- (instancetype) initWithID: (NSString*) userID andName: (NSString*) name andAge: (int) age {
    // initWithID:andName -> 這是上面的 function name，objectC 很特別啊
    self = [super init];
    if (self) {
        _userID = userID;
        _userName = name;
        _userAge = age;
    }
    
    return self;
}

@end
