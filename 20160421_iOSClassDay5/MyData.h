//
//  MyData.h
//  20160421_iOSClassDay5
//
//  Created by ChenSean on 4/21/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyData : NSObject

@property (readonly) NSString *userID;
@property (readonly) NSString *userName;
@property (readonly) int userAge;
- (instancetype) initWithID: (NSString*) userID andName: (NSString*) name andAge: (int) age;

@end
